package gitlab

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

type Namespace struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
	Path string `json:"path"`
	Kind string `json:"kind"`
}

type Project struct {
	Id int `json:"id"`
	// Default_branch         string    `json:"default_branch"`
	Archived            bool      `json:"archived"`
	Visibility          string    `json:"visibility"`
	Ssh_url_to_repo     string    `json:"ssh_url_to_repo"`
	Http_url_to_repo    string    `json:"http_url_to_repo"`
	Web_url             string    `json:"web_url"`
	Name                string    `json:"name"`
	Name_with_namespace string    `json:"name_with_namespace"`
	Path                string    `json:"path"`
	Path_with_namespace string    `json:"path_with_namespace"`
	Namespace           Namespace `json:"namespace"`
	Empty_repo          bool      `json:"empty_repo"`
}

func (c *Client) GetProjects(groupID string, projectsChan chan *Project, projectRegex string, pathRegex string) error {
	var builder strings.Builder
	req := &http.Request{}

	req.Header = http.Header{
		"PRIVATE-TOKEN": {c.Token},
	}

	builder.WriteString("/")
	builder.WriteString(APIPath)
	builder.WriteString("/groups/")
	builder.WriteString(groupID)
	builder.WriteString("/projects")

	apiURL := &url.URL{
		Path:   builder.String(),
		Host:   Host,
		Scheme: Scheme,
	}

	req.URL = apiURL

	it := 1

	for {
		q := &url.Values{}
		q.Add("page", strconv.Itoa(it))
		req.URL.RawQuery = q.Encode()

		resp, err := c.HTTPClient.Do(req)
		if err != nil {
			return err
		}

		respBody, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		var projects []*Project
		if err := json.Unmarshal(respBody, &projects); err != nil {
			return err
		}

		for _, project := range projects {
			if regexp.MustCompile(projectRegex).MatchString(project.Name) && regexp.MustCompile(pathRegex).MatchString(project.Path_with_namespace) && !project.Empty_repo && !project.Archived {
				projectsChan <- project
			}
		}

		if resp.Header.Get("x-next-page") == "" {
			break
		}
		it++
	}

	close(projectsChan)
	return nil
}
