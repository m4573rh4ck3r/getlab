package gitlab

import (
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"
)

type PersonalAccessToken struct {
	Revoked   bool        `json:"revoked"`
	Active    bool        `json:"active"`
	ExpiresAt interface{} `json:"expires_at"`
}

func (c *Client) getPersonalAcessToken() (*PersonalAccessToken, error) {
	var builder strings.Builder
	req := &http.Request{}

	req.Header = http.Header{
		"PRIVATE-TOKEN": {c.Token},
	}

	builder.WriteString("/")
	builder.WriteString(APIPath)
	builder.WriteString("/personal_access_tokens/self")

	apiURL := &url.URL{
		Path:   builder.String(),
		Host:   Host,
		Scheme: Scheme,
	}

	req.URL = apiURL

	reqDump, err := httputil.DumpRequest(req, true)
	if err != nil {
		return nil, err
	}

	slog.Debug(string(reqDump))

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	respDump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		return nil, err
	}

	slog.Debug(string(respDump))

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var personalAccessToken *PersonalAccessToken
	err = json.Unmarshal(respBody, &personalAccessToken)

	return personalAccessToken, err
}

func (p *PersonalAccessToken) validate() error {
	if !p.Active {
		return fmt.Errorf("access token is invalid")
	}

	if p.Revoked {
		return fmt.Errorf("access token has been revoked")
	}

	if p.ExpiresAt != nil {
		v, err := time.Parse("2006-01-02", p.ExpiresAt.(string))
		if err != nil {
			return fmt.Errorf("failed to parse expiry date: %v", err)
		}

		if v.Before(time.Now()) {
			return fmt.Errorf("access token has expired")
		}
	}

	return nil
}
