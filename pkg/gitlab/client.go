package gitlab

import (
	"log/slog"
	"net/http"
	"os"
)

type Client struct {
	Token      string
	HTTPClient *http.Client
}

func NewClient(token string) *Client {
	c := &Client{
		Token:      token,
		HTTPClient: &http.Client{},
	}
	p, err := c.getPersonalAcessToken()
	if err != nil {
		slog.Error("failed to validate personal access token", slog.String("error", err.Error()))
		os.Exit(1)
	}

	if err := p.validate(); err != nil {
		slog.Error("error validating the personal gitlab access token", slog.String("error", err.Error()))
		os.Exit(1)
	}

	return c
}
