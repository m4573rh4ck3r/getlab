package gitlab

import (
	"os"
	"strconv"
	"testing"
)

func TestGetGroupIDFromName(t *testing.T) {
	token := os.Getenv("gitlab_token")
	if token == "" {
		t.Skip("gitlab_token environment variable not set")
	}
	parent := os.Getenv("parent_name")
	if parent == "" {
		t.Skip("parent_name environment variable not set")
	}
	c := NewClient(token)
	id, err := c.GetGroupIDFromName(parent)
	if err != nil {
		t.Fatal(err)
	}
	if id == nil {
		t.Fatal("received empty gitlab group ID")
	}
}

func TestGetGroups(t *testing.T) {
	token := os.Getenv("gitlab_token")
	if token == "" {
		t.Skip("gitlab_token environment variable not set")
	}
	parent := os.Getenv("parent_name")
	if parent == "" {
		t.Skip("parent_name environment variable not set")
	}
	c := NewClient(token)
	id, err := c.GetGroupIDFromName(parent)
	if err != nil {
		t.Fatal(err)
	}
	groupsChan := make(chan *Group)
	go func() {
		c.GetGroups(strconv.Itoa(*id), groupsChan, "", "")
	}()
	group, ok := <-groupsChan
	if !ok {
		t.Fatal("failed to receive any groups before groups channel was closed")
	}
	if group == nil {
		t.Fatal("received empty group")
	}
}
