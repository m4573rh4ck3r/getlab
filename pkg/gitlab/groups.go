package gitlab

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

type Group struct {
	ID                                int    `json:"id"`
	Name                              string `json:"name"`
	Path                              string `json:"path"`
	Description                       string `json:"description"`
	Visibility                        string `json:"visibility"`
	Share_with_group_lock             bool   `json:"share_with_group_lock"`
	Require_two_factor_authentication bool   `json:"require_two_factor_authentication"`
	Two_factor_grace_period           int    `json:"two_factor_grace_period"`
	Project_creation_level            string `json:"project_creation_level"`
	Auto_devops_enabled               bool   `json:"auto_devops_enabled"`
	Subgroup_creation_level           string `json:"subgroup_creation_level"`
	Emails_disabled                   bool   `json:"emails_disabled"`
	Mentions_disabled                 bool   `json:"mentions_disabled"`
	Lfs_enabled                       bool   `json:"lfs_enabled"`
	Default_branch_protection         int    `json:"default_branch_protection"`
	Avatar_url                        string `json:"avatar_url"`
	Web_url                           string `json:"web_url"`
	Request_access_enabled            bool   `json:"request_access_enabled"`
	Full_name                         string `json:"full_name"`
	Full_path                         string `json:"full_path"`
	File_template_project_id          int    `json:"file_template_project_id"`
	Parent_id                         int    `json:"parent_id"`
	Created_at                        string `json:"created_at"`
}

func (c *Client) GetGroupIDFromName(parentName string) (*int, error) {
	var searchBuilder strings.Builder
	req := &http.Request{}

	req.Header = http.Header{
		"PRIVATE-TOKEN": {c.Token},
	}
	searchBuilder.WriteString("/")
	searchBuilder.WriteString(APIPath)
	searchBuilder.WriteString("/groups/")
	apiURL := &url.URL{
		Path:   searchBuilder.String(),
		Host:   Host,
		Scheme: Scheme,
	}
	req.URL = apiURL
	q := &url.Values{}
	q.Add("search", parentName)
	q.Add("top_level_only", "true")
	req.URL.RawQuery = q.Encode()

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var groups []*Group

	if err := json.Unmarshal(respBody, &groups); err != nil {
		return nil, err
	}

	if len(groups) == 0 {
		return nil, fmt.Errorf("%s: couldn't find any gitlab groups or organizations matching the pattern.", parentName)
	}

	if len(groups) > 1 {
		return nil, fmt.Errorf("%s: found multiple top level gitlab groups or organizations matching the provided pattern. Please provide a unique one.", parentName)
	}
	return &groups[0].ID, nil
}

func (c *Client) GetGroups(parentID string, groupsChan chan *Group, groupRegex string, pathRegex string) error {
	defer close(groupsChan)
	var builder strings.Builder
	req := &http.Request{}

	req.Header = http.Header{
		"PRIVATE-TOKEN": {c.Token},
	}

	builder.WriteString("/")
	builder.WriteString(APIPath)
	builder.WriteString("/groups/")
	builder.WriteString(parentID)
	builder.WriteString("/descendant_groups")

	apiURL := &url.URL{
		Path:   builder.String(),
		Host:   Host,
		Scheme: Scheme,
	}

	req.URL = apiURL

	it := 1
	for {
		q := &url.Values{}
		q.Add("page", strconv.Itoa(it))
		req.URL.RawQuery = q.Encode()

		resp, err := c.HTTPClient.Do(req)
		if err != nil {
			return err
		}

		respBody, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		var groups []*Group

		if err := json.Unmarshal(respBody, &groups); err != nil {
			return err
		}

		for _, group := range groups {
			if regexp.MustCompile(groupRegex).MatchString(group.Name) && regexp.MustCompile(pathRegex).MatchString(group.Full_path) {
				groupsChan <- group
			}
		}

		if resp.Header.Get("x-next-page") == "" {
			break
		}
		it++
	}

	return nil
}
