package gitlab

import (
	"os"
	"testing"
)

func TestNewClient(t *testing.T) {
	token := os.Getenv("gitlab_token")
	c := NewClient(token)
	if c == nil {
		t.Fatal("received empty client")
	}
}
