package gitlab

const (
	APIPath = "api/v4"
	Host    = "gitlab.com"
	Scheme  = "https"
)
