package gitlab

import (
	"fmt"
	"os"
	"strconv"
	"testing"
)

func TestGetProjects(t *testing.T) {
	token := os.Getenv("gitlab_token")
	if token == "" {
		t.Skip("gitlab_token environment variable not set")
	}
	parent := os.Getenv("parent_name")
	if parent == "" {
		t.Skip("parent_name environment variable not set")
	}
	c := NewClient(token)
	id, err := c.GetGroupIDFromName(parent)
	if err != nil {
		t.Fatal(err)
	}

	groupsChan := make(chan *Group)

	go func() {
		c.GetGroups(strconv.Itoa(*id), groupsChan, "", "")
	}()

	successChan := make(chan bool, 1)
	errChan := make(chan error, 1)

	go func() {
		for {
			projectsChan := make(chan *Project)
			group, ok := <-groupsChan
			if !ok {
				break
			}
			go func() {
				if err := c.GetProjects(strconv.Itoa(group.ID), projectsChan, "", ""); err != nil {
					fmt.Fprintln(os.Stderr, err)
					os.Exit(1)
				}
			}()
			project, ok := <-projectsChan
			if !ok {
				break
			}
			if project != nil {
				successChan <- true
			}
		}
	}()

	for {
		select {
		case _, _ = <-successChan:
			break
		case err, _ = <-errChan:
			t.Fatal(err)
		}
		break
	}
}
