package git

import (
	"errors"
	"fmt"
	"log/slog"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/m4573rh4ck3r/getlab/pkg/gitlab"
)

func Clone(directory string, project *gitlab.Project, dryRun bool) error {
	var b strings.Builder
	b.WriteString(directory)
	b.WriteString("/gitlab.com/")
	b.WriteString(project.Path_with_namespace)

	_, err := os.Stat(b.String())
	if err == nil {
		slog.Warn(fmt.Sprintf("directory %s already exists. Skipping repository %s ...", b.String(), project.Ssh_url_to_repo))
		return nil
	}

	if !errors.Is(err, os.ErrNotExist) {
		return fmt.Errorf("failed to read directory %s: %v", b.String(), err)
	}

	if dryRun {
		slog.Info(fmt.Sprintf("repository %s would be cloned into directory %s", project.Ssh_url_to_repo, b.String()))
		return nil
	}

	slog.Info(fmt.Sprintf("cloning repository %s into directory %s ...", project.Ssh_url_to_repo, b.String()))
	gitCloneCmd := exec.Command("git", "clone", project.Ssh_url_to_repo, b.String())
	if err := gitCloneCmd.Run(); err != nil {
		slog.Error(fmt.Sprintf("failed to clone git repository %s into %s: %v", project.Ssh_url_to_repo, b.String(), err))
		return nil
	}

	slog.Info(fmt.Sprintf("successfully cloned repository %s into directory %s", project.Ssh_url_to_repo, b.String()))
	return nil
}
