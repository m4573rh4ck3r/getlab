# set target and source variables

bin_PROGRAMS = getlab

getlab_SOURCES = cmd/getlab/getlab.go
getlab_DEPENDENCIES = libs

ifeq ($(target_architectures),)
	target_architectures := linux/amd64 darwin/amd64 darwin/arm64
endif


# set go env variables

ifeq ($(CGO_ENABLED),)
	CGO_ENABLED = 1
endif

ifeq ($(GOPATH),)
	GOPATH := $(HOME)/go
endif

ifeq ($(GOBIN),)
	GOBIN := $(GOPATH)/bin
endif

ifeq ($(GOOS),)
	GOOS = linux
endif

ifeq ($(GOARCH),)
	GOBIN = amd64
endif


# set compiler variables and flags

ifeq ($(cc),)
	cc = go
endif

ifeq ($(CCLD),)
	CCLD = $(cc) build
endif

ifeq ($(ldflags),)
	ldflags = -w -s
	ldflags += -X 'gitlab.com/m4573rh4ck3r/getlab/internal/cmd.version=v0.0.1'
endif

ifeq ($(installflags),)
	installflags = -s
	installflags += -D
endif

ifeq ($(verbose),)
	verbose = true
endif

ifeq ($(buildflags),)
ifeq ($(verbose),true)
	buildflags = -x
endif
endif

ifeq ($(installflags),)
ifeq ($(verbose),true)
	installflags = -v
endif
endif

ifeq ($(LiNK),)
	LINK = CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS) GOARCH=$(GOARCH) $(CCLD) $(buildflags) -ldflags "$(ldflags)"
endif


# set directory variables

srcdir := $(CURDIR)

ifeq ($(prefix),)
	prefix = /usr
endif

ifeq ($(bindir),)
	bindir := bin
endif

ifeq ($(builddir),)
	builddir = bin
endif

ifeq ($(distdir),)
	distdir = _dist
endif

ifeq ($(confdir),)
	confdir := $(prefix)/etc/$(bin_PROGRAMS)
endif


# static targets

all: build

build: $(builddir)
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), $(LINK) -o "$(builddir)/$(bin_PROGRAM)" "$($(bin_PROGRAM)_SOURCES)";)

dist: $(distdir)
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), $(foreach target_arch,$(target_architectures), $(MAKE) bin="$(bin_PROGRAM)-$(shell echo $(target_arch) | cut -d'/' -f1)-$(shell echo $(target_arch) | cut -d'/' -f2)" GOOS=$(shell echo $(target_arch) | cut -d'/' -f1) GOARCH=$(shell echo $(target_arch) | cut -d'/' -f2) build;);)
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS) $(foreach target_arch,$(target_architectures), tar cfz "$(distdir)/$(bin_PROGRAM)-$(subst /,-,$(target_arch)).tar.gz" "$(builddir)/$(bin_PROGRAM)-$(subst /,-,$(target_arch))";);)

libs:
	@GODEBUG=installgoroot=all go install std

clean:
	@rm -rvf "$(builddir)"
	@rm -rvf "$(distdir)"
	@$(cc) clean -cache

install:
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), install $(installflags) "$(builddir)/$(bin_PROGRAM)" "$(prefix)/$(bindir)";)

uninstall:
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), rm -rvf "$(prefix)/$(bindir)/$(bin_PROGRAM)";)

.PHONY: all build clean dist install libs uninstall


# dynamic targets

$(builddir):
	@mkdir -p $@

$(distdir):
	@mkdir -p $@
