package cmd

import (
	"runtime"

	"github.com/spf13/cobra"
)

var (
	parralel int
)

func init() {
	rootCmd.PersistentFlags().IntVar(&parallel, "parallel", runtime.GOMAXPROCS(0), "maximum number of concurrent goroutines")

	rootCmd.AddCommand(
		cloneCmd,
	)
}

var rootCmd = &cobra.Command{
	Use: "getlab",
}
