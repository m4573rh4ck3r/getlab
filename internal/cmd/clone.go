package cmd

import (
	"context"
	"flag"
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"

	"github.com/spf13/cobra"
	"golang.org/x/sync/semaphore"

	"gitlab.com/m4573rh4ck3r/getlab/pkg/git"
	"gitlab.com/m4573rh4ck3r/getlab/pkg/gitlab"
)

func init() {
	cloneCmd.Flags().StringVarP(&token, "token", "t", "", "gitlab token to use for authentication")
	cloneCmd.Flags().StringVarP(&parentGroupName, "parent-name", "n", os.Getenv("GETLAB_parent_name"), "name of the gitlab parent group")
	cloneCmd.Flags().IntVarP(&parentGroupId, "parent-id", "i", -1, "id of the gitlab parent group")
	cloneCmd.Flags().StringVarP(&projectRegex, "projects", "p", ".*", "regex used to match project names")
	cloneCmd.Flags().StringVarP(&groupRegex, "groups", "g", ".*", "regex used to match group names")
	cloneCmd.Flags().StringVar(&pathRegex, "paths", ".*", "regex used to match the full gitlab path")
	cloneCmd.Flags().BoolVar(&dryRun, "dry-run", true, "only print what would be done")
	cloneCmd.Flags().IntVarP(&verbose, "verbose", "v", 0, "verbosity")
}

var cloneCmd = &cobra.Command{
	Use: "clone",
	Run: func(cmd *cobra.Command, args []string) {
		logLevel := slog.Level(verbose)
		handler := slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			Level: logLevel,
			ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == slog.TimeKey {
					a.Value = slog.StringValue(time.Now().Format("15:04:05.000"))
				}

				return a
			},
		})
		slog.SetDefault(slog.New(handler))

		if token == "" && os.Getenv("gitlab_token") != "" {
			token = os.Getenv("gitlab_token")
		}
		if token == "" && os.Getenv("GITLAB_TOKEN") != "" {
			token = os.Getenv("GITLAB_TOKEN")
		}

		if token == "" {
			slog.Error("please provide a gitlab token by setting the gitlab_token or GITLAB_TOKEN environment variable or by using the --token flag.")
			os.Exit(1)
		}

		cmdFlags := make(map[string]bool)
		flag.Visit(func(f *flag.Flag) { cmdFlags[f.Name] = true })

		if cmdFlags["parent-name"] && cmdFlags["parent-id"] {
			slog.Error("parent-name and parent-id are mutually exclusive.")
			os.Exit(1)
		}

		sem := semaphore.NewWeighted(int64(parallel))
		ctx := context.Background()

		f, err := filepath.Abs(flag.Arg(0))

		if err != nil {
			slog.Error(err.Error())
			os.Exit(1)
		}

		file, err := os.Stat(f)

		if os.IsNotExist(err) {
			if !file.IsDir() {
				slog.Error(fmt.Sprintf("%s already exists, but is not a directory", f))
				os.Exit(1)
			}
			if dryRun {
				slog.Info(fmt.Sprintf("base directory %s would be created.", f))
			}

			if err := os.MkdirAll(f, os.ModePerm); err != nil {
				slog.Error("failed to create directory", slog.String("directory", f), slog.String("error", err.Error()))
				os.Exit(1)
			}
		} else if err != nil {
			slog.Error("failed to get directory", slog.String("directory", f), slog.String("error", err.Error()))
			os.Exit(1)
		}

		slog.Info("base directory already exists. Skipping creation...", slog.String("directory", f))

		client := gitlab.NewClient(token)

		if parentGroupName != "" {
			var err error
			id, err := client.GetGroupIDFromName(parentGroupName)
			if err != nil {
				slog.Error(err.Error())
				os.Exit(1)
			}
			parentGroupId = *id
		}

		if parentGroupId == -1 {
			slog.Error("please provide the ID or name of the top level gitlab group or organization by setting the environment variable parent_id, via the --parent-id command line flag, parent_name environment variable or --parent-name command line flag. (ID takes precedence over name.)")
			os.Exit(1)
		}

		var wg sync.WaitGroup
		groupsChan := make(chan *gitlab.Group)
		wg.Add(1)
		go func() {
			defer wg.Done()
			if err := client.GetGroups(strconv.Itoa(parentGroupId), groupsChan, groupRegex, pathRegex); err != nil {
				slog.Error("failed to get subgroups", slog.String("error", err.Error()))
				os.Exit(1)
			}
		}()

		for {
			group, ok := <-groupsChan
			if !ok {
				break
			}
			// retrieve all projects in group
			projectsChan := make(chan *gitlab.Project)
			wg.Add(1)
			go func() {
				defer wg.Done()
				if err := client.GetProjects(strconv.Itoa(group.ID), projectsChan, projectRegex, pathRegex); err != nil {
					slog.Error("failed to get projects", slog.String("group", group.Path), slog.String("error", err.Error()))
					os.Exit(1)
				}
			}()
			// clone repositories
			go func() {
				for {
					project, ok := <-projectsChan
					if !ok {
						break
					}
					if err := sem.Acquire(ctx, 1); err != nil {
						slog.Error("failed to aquire semlock", slog.String("error", err.Error()))
						os.Exit(1)
					}
					go func() {
						defer sem.Release(1)
						if err := git.Clone(f, project, dryRun); err != nil {
							slog.Error("failed to clone git repository", slog.String("error", err.Error()))
							os.Exit(1)
						}
					}()
				}
			}()
		}
		wg.Wait()
		if err := sem.Acquire(ctx, int64(parallel)); err != nil {
			slog.Error("failed to acquire semaphore", slog.String("error", err.Error()))
			os.Exit(1)
		}
	},
}
