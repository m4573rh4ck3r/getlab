package cmd

var (
	dryRun          bool
	errThreshold    int
	groupRegex      string
	parallel        int
	parentGroupId   int
	parentGroupName string
	pathRegex       string
	projectRegex    string
	token           string
	verbose         int
)

func Execute() {
	rootCmd.Execute()
}
