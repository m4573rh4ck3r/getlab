package main

import (
	"gitlab.com/m4573rh4ck3r/getlab/internal/cmd"
)

func main() {
	cmd.Execute()
}
